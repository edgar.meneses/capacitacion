
const healthMessage = {
  message: 'Backend clients running success',
  code: 200,
};

module.exports = {
  healthMessage,
};
