
const express = require('express');

const router = express.Router();
const { health } = require('./controller');

router.get('/', health);

module.exports = router;
