const origin = '*';
module.exports = {
  origin,
  optionsSuccessStatus: 200,
  method: 'GET, POST, PUT, PATCH, DELETE',
};
