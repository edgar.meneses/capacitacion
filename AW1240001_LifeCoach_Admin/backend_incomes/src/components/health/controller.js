
const { healthMessage } = require('./responses');

async function health(req, res) {
  const { code, message } = healthMessage;
  res.status(code)
    .json({ message });
}

module.exports = {
  health,
};
