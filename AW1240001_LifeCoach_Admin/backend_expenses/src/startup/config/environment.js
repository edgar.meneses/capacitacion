module.exports = {
  env: {
    doc: 'Its the application execution environment variable.',
    format: ['dev', 'qa', 'pdn', 'local'],
    default: 'dev',
    env: 'NODE_ENV',
  },
  port: {
    doc: 'Its the port of execution the application',
    format: 'Number',
    default: 9681,
    env: 'PORT',
  },
};
