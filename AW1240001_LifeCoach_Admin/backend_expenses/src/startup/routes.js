
const helmet = require('helmet');
const bodyParser = require('body-parser');
const cors = require('cors');
const corsOptions = require('./config/corsOptions');
const health = require('../components/health');

module.exports = (app) => {
  app.use(helmet());
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(bodyParser.json());
  app.use(cors(corsOptions));
  // Se define la ruta /health
  app.use('/health', health);
};
