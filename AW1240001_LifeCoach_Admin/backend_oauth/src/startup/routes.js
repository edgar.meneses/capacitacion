
const helmet = require('helmet');
const bodyParser = require('body-parser');
const cors = require('cors');
const corsOptions = require('./config/corsOptions');
const health = require('../components/health');
const {
  errorMiddleware,
  notFoundMiddleware,
} = require('../middleware/errors');

module.exports = (app) => {
  app.use(helmet());
  // parse application/x-www-form-urlencoded
  app.use(bodyParser.urlencoded({ extended: false }));
  // parse application/json
  app.use(bodyParser.json());
  app.use(cors(corsOptions));
  app.use('/health', health);
  app.use(errorMiddleware);
  app.use(notFoundMiddleware);
};
