
const healthMessage = {
  message: 'Backend oauth running success',
  code: 200,
};

module.exports = {
  healthMessage,
};
